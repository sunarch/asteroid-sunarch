# webserver: fenko

## subdomain

```sh
uberspace web domain add fenko.sunarch.dev
uberspace web domain list
```

## add web backend

With subpath (initial):

```sh
uberspace web backend set /fenko --http --port 1323 --remove-prefix
```

With subdomain:

```sh
uberspace web backend set fenko.sunarch.dev --http --port 1323
uberspace web backend list
```

## supervisord service

- config file in `~/etc/services.d/fenko.ini`

### initial

```sh
supervisorctl reread
supervisorctl update
```

### maintenance

```sh
supervisorctl start fenko
supervisorctl stop fenko
supervisorctl restart fenko
supervisorctl status
supervisorctl tail fenko
supervisorctl tail fenko stderr
```
