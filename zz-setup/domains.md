# domains

```sh
uberspace web domain list
```

## (default)

`sunarch.uber.space`

## sunarch.dev

```sh
uberspace web domain add sunarch.dev
```
