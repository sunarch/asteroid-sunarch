# Git repos

### Agent

```
eval $(ssh-agent -s)
```

### add key

```
ssh-add ~/.ssh/id_rsa
```

### list keys

```
ssh-add -l
```

### related

- StackOverflow: [Git: How to solve Permission denied (publickey) error when using Git?](https://stackoverflow.com/questions/2643502/git-how-to-solve-permission-denied-publickey-error-when-using-git)
